'use strict';
/*
1、题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2

function indexOf(arr, item) {

}
*/

function indexOf(arr, item) {
    return arr.indexOf(item);
  }
    console.log(indexOf([ 1, 2, 3, 4 ], 3))

   
/*
2、题目描述
计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/
function sum(arr) {
    return arr.reduce(function(total,num){
        return total + num;
    })
  
}


console.log(sum([ 1, 2, 3, 4 ]))



/*
3、题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4, 2], 2
输出

[1, 3, 4]

function remove(arr, item) {

}
*/

function remove(arr, item) {
    var result = [];
    arr.forEach(function(arrItem){
        if(item != arrItem){
            result.push(arrItem);
        }
    })
    return result;
}
console.log(remove([1, 2, 3, 4, 2], 2))
/*
4、题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
示例1
输入

[1, 2, 2, 3, 4, 2, 2], 2
输出

[1, 3, 4]

function removeWithoutCopy(arr, item) {

}
*/

function removeWithoutCopy(arr, item) {
    while(arr.indexOf(item) != -1){
        arr.splice(arr.indexOf(item),1)
    }
    return arr;
}


console.log(removeWithoutCopy([1, 2, 2, 3, 4, 2, 2], 2))

/*
5、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/
function append(arr, item) {
    var result = [];
    arr.forEach(function(arrItem){
        result.push(arrItem);
    })
    result.push(item);
    return result;
}

console.log(append([1, 2, 3, 4],  10))
/*
6、题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 2, 3]

function truncate(arr) {

}
*/
function truncate(arr) {
    return arr.slice(0,arr.length-1)

}
    console.log(truncate([1, 2, 3, 4]))

/*
7、题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组n,
示例1
输入

[1, 2, 3, 4], 10
输出

[10, 1, 2, 3, 4]

function prepend(arr, item) {

}
*/
function prepend(arr, item) {
    return[item].concat(arr);
}
console.log(prepend([1, 2, 3, 4], 10))
/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/

function curtail(arr) {
    var result = arr.slice(0);
    result.shift();
    return result;
}


console.log(curtail([1, 2, 3, 4]))
/*
9、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出z

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
function concat(arr1, arr2) {
    var currentArr1 = arr1.slice(0);
    var currentArr2 = arr2.slice(0);
    return currentArr1.concat(currentArr2)
}
console.log(concat([1, 2, 3, 4], ['a', 'b', 'c', 1]))
/*
10、题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 'z', 2
输出

[1, 2, 'z', 3, 4]

function insert(arr, item, index) {

}
*/

function insert(arr, item, index) {
    let arr1=arr.slice(0);
    arr1.splice(index,0,item);
    return arr1;
}

console.log(insert([1, 2, 3, 4], 'z', 2))
/*
11、题目描述
统计数组 arr 中值等于 item 的元素出现的次数
示例1
输入

[1, 2, 4, 4, 3, 4, 3], 4
输出

3

function count(arr, item) {

}
*/
function count(arr, item) {
    var num = 0;
    arr.forEach(function(arrItem){
        if(item === arrItem){
            num ++;
        }
    })
    return num;
}
    console.log(count([1, 2, 4, 4, 3, 4, 3], 4))
/*
12、题目描述
找出数组 arr 中重复出现过的元素
示例1
输入

[1, 2, 4, 4, 3, 3, 1, 5, 3]
输出

[1, 3, 4]

function duplicates(arr) {

}
*/
function duplicates(arr) {
    let result=[];
    arr.forEach(function(item) {
        if(arr.indexOf(item) != arr.lastIndexOf(item)&&result.indexOf(item) ==-1){
            result.push(item);
        }
        
    });
    return result;
}

console.log(duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3]))
/*
13、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/

function square(arr) {
    return arr.map(item =>{
        return item*item
    })
}
console.log(square([1, 2, 3, 4]))
/*
14、题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
示例1
输入

['a','b','c','d','e','f','a','b','c'] 'a'
输出

[0, 6]

function findAllOccurrences(arr, target) {

}
*/
function findAllOccurrences(arr, target) {
    let arr1=[];
    let j=0;
    for(let i=0;i<arr.length;i++){
        if(arr[i]===target){
            arr1.push(arr.indexOf(arr[i],j));
            j++;
        }
    }
    return arr1;
}
console.log(findAllOccurrences(['a','b','c','d','e','f','a','b','c'] ,'a'))
/*
15、题目描述
判断 val1 和 val2 是否完全等同

function identity(val1, val2) {

}
*/

function identity(val1, val2) {
    return val1 === val2
}
console.log(identity(1,'1'));
/*
16、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
/*
function area_of_circle(r, pi) {
	//**先用关键字arguments判断一下输入的参数个数**
 	if (arguments.length === 1){
        var pi = 3.14;//如果没有输入pi的值，就给pi赋值
}   
    return r*r*pi;//返回计算结果
}
 var p=pi || 3.14
 return p*r*r

var result=area_of_circle(2)
console.log(result);
*/


/*
17、题目描述

<!-- HTML结构 -->
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>

针对以上文档结构，请把与Web开发技术不相关的节点删掉：

注意！！请分别使用原生JS和jQuery两种试实现！！！！！

*/


/*
18、题目描述

对如下的Form表单：

<!-- HTML结构 -->
<form id="test-form" action="test">
    <legend>请选择想要学习的编程语言：</legend>
    <fieldset>
        <p><label class="selectAll"><input type="checkbox"> <span class="selectAll">全选</span><span class="deselectAll">全不选</span></label> <a href="#0" class="invertSelect">反选</a></p>
        <p><label><input type="checkbox" name="lang" value="javascript"> JavaScript</label></p>
        <p><label><input type="checkbox" name="lang" value="python"> Python</label></p>
        <p><label><input type="checkbox" name="lang" value="ruby"> Ruby</label></p>
        <p><label><input type="checkbox" name="lang" value="haskell"> Haskell</label></p>
        <p><label><input type="checkbox" name="lang" value="scheme"> Scheme</label></p>
		<p><button type="submit">Submit</button></p>
    </fieldset>
</form>

要求:
绑定合适的事件处理函数，实现以下逻辑：

当用户勾上“全选”时，自动选中所有语言，并把“全选”变成“全不选”；

当用户去掉“全不选”时，自动不选中所有语言；

当用户点击“反选”时，自动把所有语言状态反转（选中的变为未选，未选的变为选中）；

当用户把所有语言都手动勾上时，“全选”被自动勾上，并变为“全不选”；

当用户手动去掉选中至少一种语言时，“全不选”自动被去掉选中，并变为“全选”。


*/