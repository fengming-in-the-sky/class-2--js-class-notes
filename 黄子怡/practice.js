//基本语法和数据类型

//练习：

//分别利用行注释和块注释把下面的语句注释掉，使它不再执行：

/*
// 请注释掉下面的语句:
alert('我不想执行');
alert('我也不想执行');
*/

//字符串和数组

//练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
'use strict';
/*
    var arr = ['小明', '小红', '大军', '阿黄'];
    arr.sort();
    var str="";
    var length=arr.length;
    if(length===0){
        str="我好像被放鸽子了";
    }else if(length===1){
        str="欢迎"+arr[0]+"同学！"
    }else if(length===2){
        str="欢迎"+arr[0]+"和"+arr[1]+"同学！"
    }else{
        var newname=arr.pop();
        str="欢迎"+arr.join(',')+"和"+newname+"同学！"
    }
    console.log(str);
*/
var arr = ['小明', '小红', '大军', '阿黄'];
arr.join(',')

console.log('欢迎'+arr.slice(0,3)+'和'+arr[3]+'同学！');
//对象、判断和循环
/*
练习
小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：

低于18.5：过轻
18.5-25：正常
25-28：过重
28-32：肥胖
高于32：严重肥胖
用if...else...判断并显示结果：
```
'use strict';

var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));
var bmi = ???;
if ...
*/

//练习
//请利用循环遍历数组中的每个名字，并显示Hello, xxx!：


//请尝试for循环和while循环，并以正序、倒序两种方式遍历。
/*
var arr = ['Bart', 'Lisa', 'Adam']
var i=0;
while(i<arr.length){
    i++
}
arr.reverse();
console.log("Hello"+arr);


var arr = ['Bart', 'Lisa', 'Adam']
arr.sort();
for (let i = 0; i < arr.length; i++) {
     console.log('Hello,'+arr[i]+'!');
    
}

arr.reverse();
for (let i = 0; i < arr.length; i++) {
    console.log('Hello,'+arr[i]+'!');
   
}*/
//函数

//因为rest参数是ES6新标准，所以你需要测试一下浏览器是否支持。请用rest参数编写一个sum()函数，接收任意个参数并返回它们的和：
/*
'use strict';
 function sum(...rest) {
    var result=0;
    for (let i = 0; i < rest.length; i++) {
        result=result+rest[i];
         
     }
     return result;
 }

// 测试:
var i, args = [];
for (i=1; i<=100; i++) {
    args.push(i);
}
if (sum() !== 0) {
    console.log('测试失败: sum() = ' + sum());
} else if (sum(1) !== 1) {
    console.log('测试失败: sum(1) = ' + sum(1));
} else if (sum(2, 3) !== 5) {
    console.log('测试失败: sum(2, 3) = ' + sum(2, 3));
} else if (sum.apply(null, args) !== 5050) {
    console.log('测试失败: sum(1, 2, 3, ..., 100) = ' + sum.apply(null, args));
} else {
    console.log('测试通过!');
}

/*
//练习
//定义一个计算圆面积的函数area_of_circle()，它有两个参数：

//r: 表示圆的半径；
//pi: 表示π的值，如果不传，则默认3.14

'use strict';

function area_of_circle(r, pi) {
    var pai=pi || 3.14
   return pai*r*r

}
// 测试:
if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}

//小明是一个JavaScript新手，他写了一个max()函数，返回两个数中较大的那个：

'use strict';

function max(a, b) {
    if (a > b) {
        return a
    } else {
        return b
    }

}//;号的问题
console.log(max(15, 20));
*/

//高阶函数和数组

//练习：利用reduce()求积：
'use strict';
 /*
function product(arr) {
    var result=arr.reduce(function (x,y) {
        return x*y;
    })
    return result;
}

// 测试:
if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
     console.log('测试通过!');
}
else {
    console.log('测试失败!');
}

/*

 //练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

'use strict';

function string2int(s) {
        function str2num(str){
            var strArr = str.split('');               //把字符串分割成字符串数组
            function toInt(data){ 
                return +data;                  //通过js的弱类型转换，实现字符类型到数字类型的转换
            }
            var numArr = strArr.map(toInt);           //通过map()把字符串数组转换成数字数组
            return numArr;
        }
        var num = str2num(s);
        var res = num.reduce(function (x,y) {        //通过reduce()把数字数组转换成数字量
            return x*10+y;
        });
        return res;
    }    


// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}

//练习
//请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。



function normalize(arr) {
   function EnglishName(name){
        var str  ='';
        for(var i=0;i<name.length;i++){
            if(i===0){
                str=str+name[i].toUpperCase()
            }else{
                str=str+name[i].toLowerCase()
            }
        }
        return str
   }
   return arr.map(EnglishName)

}
*/
function normalize(arr) {
    function EnglishName(name) {
        var 
    
    }
}

// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}


//小明希望利用map()把字符串变成整数，他写的代码很简洁：


var arr = ['1', '2', '3'];
var r;
r = arr.map(function(x){
    return parseInt(x)
});

console.log(r);



//结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。

//练习 请尝试用filter()筛选出素数：

'use strict';

function get_primes(arr) {
    return [];

}

// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
//练习 小明为了和女友庆祝情人节，特意制作了网页，并提前预定了法式餐厅。小明打算用JavaScript给女友一个惊喜留言：
/*
'use strict';
var today = new Date();
if (today.getMonth() === 2 && today.getDate() === 14) {
    alert('亲爱的，我预定了晚餐，晚上6点在餐厅见！');
}
结果女友并未出现。小明非常郁闷，请你帮忙分析他的JavaScript代码有何问题。

//浏览器和常见操纵
/*
练习
有如下的HTML结构：

javascript

Java

<!-- HTML结构 -->
<div id="test-div">
  <p id="test-js">javascript</p>
  <p>Java</p>
</div>
请尝试获取指定节点并修改：
*/
/*
'use strict';
// 获取<p>javascript</p>节点:
var js =  document.getElementById('test-js')
  
// 修改文本为JavaScript:
// TODO: 
js.innerHTML='JavaScript';
// 修改CSS为: color: #ff0000, font-weight: bold
// TODO:
js.style.color ='#ff0000';
js.style.fontWeight='bold'
 
// 测试:
if (js && js.parentNode && js.parentNode.id === 'test-div' && js.id === 'test-js') {
    if (js.innerText === 'JavaScript') {
        if (js.style && js.style.fontWeight === 'bold' && (js.style.color === 'red' || js.style.color === '#ff0000' || js.style.color === '#f00' || js.style.color === 'rgb(255, 0, 0)')) {
            console.log('测试通过!');
        } else {
            console.log('CSS样式测试失败!');
        }
    } else {
        console.log('文本测试失败!');
    }
} else {
    console.log('节点测试失败!');
}


/*
练习
对于一个已有的HTML结构：
```
Scheme
JavaScript
Python
Ruby
Haskell
<!-- HTML结构 -->
<ol id="test-list">
    <li class="lang">Scheme</li>
    <li class="lang">JavaScript</li>
    <li class="lang">Python</li>
    <li class="lang">Ruby</li>
    <li class="lang">Haskell</li>
</ol>
```
按字符串顺序重新排序DOM节点：
```
'use strict';
// sort list:

// 测试:
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i=0; i<t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();

*/
/*
'use strict';
// sort list:
//获取父节点
var list =document.getElementById("test-list");
var arr = [];
// console.log(list);

//将父节点的孩子放进array数组中，js中数组元素的类型可以任意，这里数组元素类型为DOM节点
for (let i = 0; i < list.children.length; i++){
    arr.push(list.children[i]);
}
// console.log(arr);

//利用高阶函数sort()对数组排序，排序规则自定义一个函数传给sort
//题目要求根据节点的文本内容按照字符串顺序排序
arr.sort(function(x, y){
    if (x.innerText > y.innerText){
        return 1;
    }else if (x.innerText < y.innerText){
        return -1;
    }else {
        return 0;  
    }
});
// console.log(arr);

//将节点添插入父节点的中

// 因为我们插入的js节点已经存在于当前的文档树，因此这个节点首先会从原先的位置删除，再插入到新的位置。
for (let i = 0; i < arr.length; i++){
    list.appendChild(arr[i]);
}

;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i=0; i<t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();

/*
练习
JavaScript
Swift
HTML
ANSI C
CSS
DirectX
```
<!-- HTML结构 -->
<ul id="test-list">
    <li>JavaScript</li>
    <li>Swift</li>
    <li>HTML</li>
    <li>ANSI C</li>
    <li>CSS</li>
    <li>DirectX</li>
</ul>
```
把与Web开发技术不相关的节点删掉：
```
'use strict';
// TODO

// 测试:
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 3) {
        arr = [];
        for (i = 0; i < t.children.length; i ++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
```
*/


/*
练习
如下的HTML结构：

JavaScript

Java

Python

Ruby

Swift

Scheme

Haskell
```
<!-- HTML结构 -->
<div id="test-div">
<div class="c-red">
    <p id="test-p">JavaScript</p>
    <p>Java</p>
  </div>
  <div class="c-red c-green">
    <p>Python</p>
    <p>Ruby</p>
    <p>Swift</p>
  </div>
  <div class="c-green">
    <p>Scheme</p>
    <p>Haskell</p>
  </div>
</div>
```
请选择出指定条件的节点：
```
'use strict';
// 选择<p>JavaScript</p>:
var js = ???;

// 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
var arr = ???;

// 选择<p>Haskell</p>:
var haskell = ???;

// 测试:
if (!js || js.innerText !== 'JavaScript') {
    alert('选择JavaScript失败!');
} else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
    console.log('选择Python,Ruby,Swift失败!');
} else if (!haskell || haskell.innerText !== 'Haskell') {
    console.log('选择Haskell失败!');
} else {
    console.log('测试通过!');
}
```
*/
/*
'use strict';
// 选择<p>JavaScript</p>:
var js =document.getElementById('test-p');

// 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
var arr =document.getElementsByClassName('c-red c-green')[0].children

// 选择<p>Haskell</p>:
var haskell =  document.getElementsByClassName('c-green')[1].lastElementChild
 


if (!js || js.innerText !== 'JavaScript') {
    alert('选择JavaScript失败!');
} else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
    console.log('选择Python,Ruby,Swift失败!');
} else if (!haskell || haskell.innerText !== 'Haskell') {
    console.log('选择Haskell失败!');
} else {
    console.log('测试通过!');
}
/*
利用JavaScript检查用户注册信息是否正确，在以下情况不满足时报错并阻止提交表单：

用户名必须是3-10位英文字母或数字；

口令必须是6-20位；

两次输入口令必须一致。
```
<!-- HTML结构 -->
<form id="test-register" action="#" target="_blank" onsubmit="return checkRegisterForm()">
    <p id="test-error" style="color:red"></p>
    <p>
        用户名: <input type="text" id="username" name="username">
    </p>
    <p>
        口令: <input type="password" id="password" name="password">
    </p>
    <p>
        重复口令: <input type="password" id="password-2">
    </p>
    <p>
        <button type="submit">提交</button> <button type="reset">重置</button>
    </p>
</form>
```
用户名: 

口令: 

重复口令: 

提交 重置
```
'use strict';
var checkRegisterForm = function () {
    // TODO:
    return false;
}

// 测试:
;(function () {
    window.testFormHandler = checkRegisterForm;
    var form = document.getElementById('test-register');
    if (form.dispatchEvent) {
        var event = new Event('submit', {
    		bubbles: true,
    		cancelable: true
  		});
        form.dispatchEvent(event);
    } else {
        form.fireEvent('onsubmit');
    }
})();
```
*/



