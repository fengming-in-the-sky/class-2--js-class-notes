

/*

计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/


function sum(arr) {
 
}


console.log(sum([ 1, 2, 3, 4 ]))



/*
1、题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2


*/

function indexOf(arr, item) {
  
}
    console.log(indexOf([ 1, 2, 3, 4 ], 3));

/*
3、题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4, 2], 2
输出

[1, 3, 4]


*/

function remove(arr, item) {
   

           
}

console.log(remove([1, 2, 3, 4, 2], 2));
/*
4、题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
示例1
输入

[1, 2, 2, 3, 4, 2, 2], 2
输出

[1, 3, 4]


*/
function removeWithoutCopy(arr, item) {
    
}
    console.log(removeWithoutCopy([1, 2, 2, 3, 4, 2, 2], 2));
/*
5、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/

function append(arr, item) {
   
}

/*
6、题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 2, 3]

function truncate(arr) {

}
*/
function truncate(arr) {
     
}
    console.log(truncate([1, 2, 3, 4]));
/*
7、题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 10
输出

[10, 1, 2, 3, 4]

function prepend(arr, item) {

}
*/

function prepend(arr, item) {

}

/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/
function curtail(arr) {

}

/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/

function curtail(arr) {
    
}


console.log(curtail([1, 2, 3, 4]))
/*
9、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出z

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
function concat(arr1, arr2) {
     
}
console.log(concat([1, 2, 3, 4], ['a', 'b', 'c', 1]))
/*
10、题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 'z', 2
输出

[1, 2, 'z', 3, 4]

function insert(arr, item, index) {

}
*/

function insert(arr, item, index) {
     
}

console.log(insert([1, 2, 3, 4], 'z', 2))
/*
11、题目描述
统计数组 arr 中值等于 item 的元素出现的次数
示例1
输入

[1, 2, 4, 4, 3, 4, 3], 4
输出

3

function count(arr, item) {

}
*/
function count(arr, item) {
    
}
    console.log(count([1, 2, 4, 4, 3, 4, 3], 4))
/*
12、题目描述
找出数组 arr 中重复出现过的元素
示例1
输入

[1, 2, 4, 4, 3, 3, 1, 5, 3]
输出

[1, 3, 4]

function duplicates(arr) {

}
*/
function duplicates(arr) {
     
}

console.log(duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3]))
/*
13、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/

function square(arr) {
    
}
console.log(square([1, 2, 3, 4]))
/*
14、题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
示例1
输入

['a','b','c','d','e','f','a','b','c'] 'a'
输出

[0, 6]

function findAllOccurrences(arr, target) {

}
*/
function findAllOccurrences(arr, target) {
     
}
console.log(findAllOccurrences(['a','b','c','d','e','f','a','b','c'] ,'a'))
/*
15、题目描述
判断 val1 和 val2 是否完全等同

function identity(val1, val2) {

}
*/

function identity(val1, val2) {
   
}
console.log(identity(1,'1'));
/*
16、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
function area_of_circle(r, pi) {
	 
}
