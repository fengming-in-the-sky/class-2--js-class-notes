
jQuery：
<body>
    <!-- HTML结构 -->
    <div id="test-jquery">
        <p id="para-1" class="color-red">JavaScript</p>
        <p id="para-2" class="color-green">Haskell</p>
        <p class="color-red color-green">Erlang</p>
        <p name="name" class="color-black">Python</p>
        <form class="test-form" target="_blank" action="#0" onsubmit="return false;">
            <legend>注册新用户</legend>
            <fieldset>
                <p><label>名字: <input name="name" value="hyr"></label></p>
                <p><label>邮件: <input name="email" value="81674944@qq.com"></label></p>
                <p><label>口令: <input name="password" type="password"></label></p>
                <p><button type="submit">注册</button></p>
            </fieldset>
        </form>
    </div>

    <script>
        'use strict';
        // 仅选择JavaScript
        let js = $('#para-1').text();
        console.log(js); // JavaScript

        // 仅选择Erlang
        let er = $('.color-red.color-green').text();
        console.log(er); // Erlang

        // 选择JavaScript和Erlang
        let jsAndEr1 = $('#para-1, .color-red.color-green').text();
        console.log(jsAndEr1); // JavaScriptErlang
        // 或者
        let jsAndEr2 = $('.color-red').text();
        console.log(jsAndEr2); // JavaScriptErlang

        // 选择所有编程语言
        let d = $('[class^=color-]').text();
        console.log(d); // JavaScriptHaskellErlangPython

        // 选择名字input
        let name = $('input[name="name"]').val();
        console.log(name); // hyr

        // 选择邮件和名字input
        let emailAndName = $('input[name="email"],input[name="name"]').val();
        console.log(emailAndName); // 81674944@qq.com hyr
    </script>
</body>
<body>
    <!-- HTML结构 -->
    <div class="testing">
        <ul class="lang">
            <li class="lang-javascript">JavaScript</li>
            <li class="lang-python">Python</li>
            <li class="lang-lua">Lua</li>
            <li class="lang-java">Java</li>
        </ul>
    </div>

    <script>
        // 仅选出JavaScript
        let js = $('ul.lang li:first-child');
        console.log(js.text()); // JavaScript

        // 仅选出Java
        let lua = $('ul.lang li:last-child');
        console.log(lua.text()); // Java

        // 选出第N个元素，N从1开始
        let nth = $('ul.lang li:nth-child(2)');
        console.log(nth.text()); // Python

        // 选出序号为偶数的元素
        let even = $('ul.lang li:nth-child(even)');
        console.log(even.text()); // PythonJava

        // 选出序号为奇数的元素
        let odd = $('ul.lang li:nth-child(odd)');
        console.log(odd.text()); // JavaScriptLua
    </script>
</body>
<body>
    <!-- HTML结构 -->
    <ul id="test-ul">
        <li class="js">JavaScript</li>
        <li name="book">Java &amp; JavaScript</li>
    </ul>
    
    <script>
        let book1 = $('#test-ul li[name="book"]');
        console.log(book1.text()); // Java & JavaScript

        let book2 = $('#test-ul li[name="book"]');
        console.log(book2.html()); // Java &amp; JavaScript

        let j1 = $('#test-ul li.js');
        let j2 = $('#test-ul li[name="book"]');
        j1.text('HYR'); //HYR
        j2.html('<span style="color: red;">HYR6</span>'); // HYR6 
        
    </script>
</body>
<body>
    <!-- HTML结构 -->
    <ul id="test-css">
        <li class="lang dy"><span>JavaScript</span></li>
        <li class="lang"><span>Java</span></li>
        <li class="lang dy"><span>Python</span></li>
        <li class="lang"><span>Swift</span></li>
        <li class="lang dy"><span>Scheme</span></li>
    </ul>
    
    <script>
        'use strict';
        let a = $('#test-css li.dy>span').css('background-color', 'pink').css('color', 'red');
        console.log(a);
    </script>
</body>
<body>
    <!-- HTML结构 -->
    <div id="test-div">
        <ul>
            <li><span>JavaScript</span></li>
            <li><span>Python</span></li>
            <li><span>Swift</span></li>
        </ul>
    </div>

    <script>
        'use strict';
        // 除了列出的3种语言外，请再添加Pascal、Lua和Ruby，然后按字母顺序排序节点：
        // 测试:

        // 1. 获取原来的元素：
        let lis = $('#test-div>ul>li');

        // 2. 将lis转换为数组：
        let oldArr = lis.map(function() {
            return $(this).text();
        }).get();

        // 3. 创建需要添加的新元素数组：
        let appendArr = ['Pascal', 'Lua', 'Ruby'];

        // 4. 将旧数组和需要添加新元素的数组合并，并分类：
        let newArr = oldArr.concat(appendArr).sort();

        // 5. 将新数组转换为字符串显示在html里：
        let strHtml = newArr.map(function(newArr) {
            return '<li><span>' + newArr + '</span></li>'
        });
        let ul = $('#test-div>ul');
        ul.html(strHtml);



        (function() {
            var s = $('#test-div>ul>li').map(function() {
                return $(this).text();
            }).get().join(',');
            if (s === 'JavaScript,Lua,Pascal,Python,Ruby,Swift') {
                console.log('测试通过!');
            } else {
                console.log('测试失败: ' + s);
            }
        })();
    </script>
</body>