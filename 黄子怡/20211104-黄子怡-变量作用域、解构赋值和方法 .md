# rest参数

rest参数用于获取函数的多余参数，实际就是替换arguments对象。
```
function add (...values){

　　let sum = 0;

　　for (var val of values) {

　　　sum += val;

　　}

　　return sum;

}
```

以上代码利用rest参数可以向该函数传入任意数目的参数

2.
// arguments的写法
```
const sortNumbers = () => Array.prototype.slice.call(arguments).sort();
```
// rest参数的写法
```
const sortNumbers = (...numbers) => numbers.sort();
```

3.rest参数中的变量代表一个数组，所以数组特有的方法都可以用于这个变量。


[例1]： 
```
function push(array, ...items) {

　　items.forEach(function(item) {

　　 　array.push(item);

　　　console.log(item);  // 输出 1    2   3

　　})

}

var a = [];

push(a, 1, 2, 3);
```
 

注意：rest参数之后不能再有其他参数(即只能是最后一个参数)，否则报错

# 变量作用域与解构赋值

作用域说明:指一个变量的作用范围                                                                                                                  
## 全局作用域
不在任何函数内定义的变量就具有全局作用域。

(1) 全局作用域在页面打开时被创建,页面关闭时被销毁

(2) 编写在script标签中的变量和函数,作用域为全局，在页面的任意位置都可以访问到

(3) 在全局作用域中有全局对象window,代表一个浏览器窗口,由浏览器创建,可以直接调用

(4) 全局作用域中声明的变量和函数会作为window对象的属性和方法保存

(5) window对象的属性和方法可以直接调用,如window.an() 可以写为 an()
```
var a = 10;
b = 20;
function an(){
    console.log('an')
}
var bn = function(){
    console.log('bn')
}
console.log(window)
```

![图裂了](../imgs/8.png)


## 名字空间
全局变量会绑定到window上，不同的JavaScript文件如果使用了相同的全局变量，或者定义了相同名字的顶层函数，都会造成命名冲突，并且很难被发现。

减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中。

[例1]：

// 唯一的全局变量MYAPP:
```
var MYAPP = {};
```
// 其他变量:
```
MYAPP.name = 'myapp';
MYAPP.version = 1.0;
```
// 其他函数:
```
MYAPP.foo = function () {
    return 'foo';
};
```
把自己的代码全部放入唯一的名字空间MYAPP中，会大大减少全局变量冲突的可能。

## 局部作用域

(1) 调用函数时,函数作用域被创建,函数执行完毕,函数作用域被销毁
```
function an(){
    var s = 'an'
    console.log(s);
}
//an();
```

此时函数an并没有执行，作用域没有创建，当函数执行时,作用域创建，输出结果an

(2) 每调用一次函数就会创建一个新的函数作用域,他们之间是相互独立的

(3) 在函数作用域中可以访问到全局作用域的变量,在函数外无法访问到函数作用域内的变量
```
function an(){
    var s = 'an'
    console.log(s);
}
an();
console.log(s);  // 此时，程序会从当前作用域和上级作用域及以上作用域中寻找变量s，并不会去下级作用域中寻找

```

(4) 在函数作用域中访问变量、函数时,会先在自身作用域中寻找,若没有找到,则会到函数的上一级作用域中寻找,一直到全局作用域
(5) 在函数作用域中也有声明提前的特性,对于变量和函数都起作用,此时函数作用域相当于一个小的全局作用域,详细声明提前请看声明提前部分

```
an();
bn();
function an(){
    var s = 'an'
    console.log(s);
}
var bn = function(){
    console.log('bn')
}
```

下图就结果中，an()可以正常执行，函数an()提升并创建了，函数bn的变量名提升了，但是为赋值，此时bn不是函数


(6) 在函数作用域中,不使用变量关键字声明的变量,在赋值时会往上一级作用域寻找已经声明的同名变量,直到全局作用域时还没找到,则会成为window的属性
```
an(); // 输出结果 bn
function an(){
    var b = 'bn';
    function bn(){
        console.log(b); 
	b = 'bn2';   // b会往上一级寻找已经声明的同名变量,并赋值,直到全局作用域时还没找到,则会成为window的属性
    }
    bn();
    console.log(b); // 输出 bn2
}
```
(7) 在函数中定义形参,等同于声明变量
```
function an(name){
    console.log(name); // 输出 undefined
}
an();
```
等同于
```
function an(){
    var name
    console.log(name); // 输出 undefined
}
an();
```