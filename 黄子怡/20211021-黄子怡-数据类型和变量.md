## JavaScript 数据类型

1.基本数据类型

a.数值数据类型（number）

b.布尔类型（boolean）

尽管字符串和数字类型可以有无数不同的值，boolean 数据类型却只有两个值。它们是文字 true 和 false。Boolean值是一个真值，它表示一个状态的有效性（说明该状态为真或假）。

```
2>1;//这是一个true值
2>=3;//这是一个false值
```
c.未定义数据类型（underfined）

d.空数据类型 （null）


2.引用数据类型

a.字符串类型（string）

字符串是以单引号或双引号括起来的任意文本，比如'abc',"xyz"。

b.Array数据组类型

数组是一种类列表对象，它的原型中提供了遍历和修改元素的相关操作。JavaScript 数组的长度和元素类型都是非固定的。因为数组的长度可随时改变，并且其数据在内存中也可以不连续，所以 JavaScript 数组不一定是密集型的，这取决于它的使用方式。

'''
[1,2,3.14,'hello',null,true];
'''
上述数组包含6个元素。数组用[]表示，元素之间用,分隔。
另一种创建数组的方法是通过Array（）函数实现：
```
new Array(1,2,3);//创建了数组[1,2,3]
```
然而，处于代码的可读性考虑，强烈建议直接使用[]。

数组的元素可以通过索引来访问，请注意，索引的起始值为0；

c.对象类型（object）

ECMA中的对象其实就是一组数据和功能的集合。对象可以通过执行new操作符后跟要创建的对象类型的名称来创建。而创建Object类型的实例并为其添加属性和（或）方法，就可以创建自定义对象，这个语法与Java中创建对象的语法相似。但在ECMA中，如果不给构造函数传递参数，则可以省略后面的那一对圆括号。如下所示：
```
var o = new Object();

var o = new Object;
```       
## 布尔值

&&运算是与运算，只有所有都为true，&&运算结果才是true：

```
true && true; // 这个&&语句计算结果为true
true && false; // 这个&&语句计算结果为false
false && true && false; // 这个&&语句计算结果为false
```
||运算是或运算，只要其中有一个为true，||运算结果就是true：
```
false || false; // 这个||语句计算结果为false
true || false; // 这个||语句计算结果为true
false || true || false; // 这个||语句计算结果为true
```
!运算是非运算，它是一个单目运算符，把true变成false，false变成true：
```
! true; // 结果为false
! false; // 结果为true
! (2 > 5); // 结果为true
```

布尔值经常用在条件判断中，比如：
```
var age = 15;
if (age >= 18) {
    alert('adult');
} else {
    alert('teenager');
}
```
4. 比较运算符
当我们对Number做比较时，可以通过比较运算符得到一个布尔值：
```
2 > 5; // false
5 >= 2; // true
7 == 7; // true
```
实际上，JavaScript允许对任意数据类型做比较：
```
false == 0; // true
false === 0; // false
```
要特别注意相等运算符==。JavaScript在设计时，有两种比较运算符：

第一种是==比较，它会自动转换数据类型再比较，很多时候，会得到非常诡异的结果；

第二种是===比较，它不会自动转换数据类型，如果数据类型不一致，返回false，如果一致，再比较。

由于JavaScript这个设计缺陷，不要使用==比较，始终坚持使用===比较。

另一个例外是NaN这个特殊的Number与所有其他值都不相等，包括它自己：
```
NaN === NaN; // false
```
唯一能判断NaN的方法是通过isNaN()函数：
```
isNaN(NaN); // true
```
最后要注意浮点数的相等比较：
```
1 / 3 === (1 - 2 / 3); // false
```
这不是JavaScript的设计缺陷。浮点数在运算过程中会产生误差，因为计算机无法精确表示无限循环小数。要比较两个浮点数是否相等，只能计算它们之差的绝对值，看是否小于某个阈值：
```
Math.abs(1 / 3 - (1 - 2 / 3)) < 0.0000001; // true
```

## 变量
1.JS中变量的命名规则

(1)变量名的第一个字符只能是英文字母或下划线

(2)变量名从第二个字符开始，可以使用数字、字母和下划线

(3)变量名区分大小写，如变量A和变量a是两个不同的变量

(4)不能使用JavaScript的关键字（保留字）

2.变量定义的方法
```
var name;
var answer=null;
var price=12.50;
var str="hello!";
var a,b,c;
result=true;
```

## null和undefined的区别

null和undefined的区别

1、首先是数据类型不一样
```
console.log(typeof null) //object

console.log(typeof undefined) //undefined
```

2、null和undefined两者相等，但是当两者做全等比较时，两者又不等。（因为他们的数据类型不一样）
```
console.log(null==undefined) //ture

console.log(null===undefined) //false
```

3、转化成数字不同
```
console.log(Number(null)) //0

console.log(Number(undefined)) //NaN

console.log(Number(22+null)) //22

console.log(Number(22+undefined)) //NaN
```

 4、null代表“空”，代表空指针；undefined是定义了没有赋值

```
var a;

console.log(a);//undefined

 

var b=null;

console.log(b);//null
```
## 对象
JavaScript 中的所有事物都是对象：字符串、数值、数组、函数...

此外，JavaScript 允许自定义对象。
所有事物都是对象
JavaScript 提供多个内建对象，比如 String、Date、Array 等等。 对象只是带有属性和方法的特殊数据类型。

布尔型可以是一个对象。
数字型可以是一个对象。
字符串也可以是一个对象
日期是一个对象
数学和正则表达式也是对象
数组是一个对象
甚至函数也可以是对象

JavaScript 对象
对象只是一种特殊的数据。对象拥有属性和方法。









## JavaScript 严格模式(use strict)

1.使用 "use strict" 指令
"use strict" 指令在 JavaScript 1.8.5 (ECMAScript5) 中新增。

它不是一条语句，但是是一个字面量表达式，在 JavaScript 旧版本中会被忽略。

"use strict" 的目的是指定代码在严格条件下执行。

严格模式下你不能使用未声明的变量。

严格模式通过在脚本或函数的头部添加 use strict; 表达式来声明。

实例中我们可以在浏览器按下 F12 (或点击"工具>更多工具>开发者工具") 开启调试模式，查看报错信息。


2.为什么使用严格模式:

a.消除Javascript语法的一些不合理、不严谨之处，减少一些怪异行为;

b.消除代码运行的一些不安全之处，保证代码运行的安全；

c.提高编译器效率，增加运行速度；

d.为未来新版本的Javascript做好铺垫。

e."严格模式"体现了Javascript更合理、更安全、更严谨的发展方向，包括IE 10在内的主流浏览器，都已经支持它，许多大项目已经开始全面拥抱它。