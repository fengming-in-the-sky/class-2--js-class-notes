# Map

Map是一组键值对的结构，具有极快的查找速度。

举个例子，假设要根据同学的名字查找对应的成绩，如果用Array实现，需要两个Array：
```
var names = ['Michael', 'Bob', 'Tracy'];
var scores = [95, 75, 85];
```

给定一个名字，要查找对应的成绩，就先要在names中找到对应的位置，再从scores取出对应的成绩，Array越长，耗时越长。

如果用Map实现，只需要一个“名字”-“成绩”的对照表，直接根据名字查找成绩，无论这个表有多大，查找速度都不会变慢。用JavaScript写一个Map如下：
```
var m = new Map([['Michael', 95], ['Bob', 75], ['Tracy', 85]]);
m.get('Michael'); // 95
 ```

初始化Map需要一个二维数组，或者直接初始化一个空Map。Map具有以下方法：

```
var m = new Map(); // 空Map
m.set('Adam', 67); // 添加新的key-value
m.set('Bob', 59);
m.has('Adam'); // 是否存在key 'Adam': true
m.get('Adam'); // 67
m.delete('Adam'); // 删除key 'Adam'
m.get('Adam'); // undefined
```
 

由于一个key只能对应一个value，所以，多次对一个key放入value，后面的值会把前面的值冲掉：
```
var m = new Map();
m.set('Adam', 67);
m.set('Adam', 88);
m.get('Adam'); // 88
```

# reduce

reduce() 方法接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值。

reduce() 可以作为一个高阶函数，用于函数的 compose。

注意: reduce() 对于空数组是不会执行回调函数的。


[例1] 利用reduce求积
```
'use strict'
function product(arr)
{
    return 0;
}
//if (product([1,2,3,4]))===24 && product([0,1,2])===0 && product([99,88,77,66])===44274384
{
    console.log('测试通过！');
}
    else {
        console.log('测试失败！');
    }
```

[例2] 不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数
```
'use strict';
 
function string2int(s) {
  var s_arr = [];
  for(let i = 0; i < s.length; i++) {
    s_arr.push(s[i]);
}
  var int_arr = s_arr.map(function(x) {
    return +x;
});
  
  return int_arr.reduce(function(x,y) {
    return x*10 + y;
});
 
}
 
// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}
```                                                                                                                
 [例3] 请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字

```
function normalize(arr) {
    function correct(word){
        var list ='';
        for(i=0;i<word.length;i++){
            if(i === 0)
                list += word[i].toUpperCase();
            else
               list += word[i].toLowerCase();
        }

        return list;
    }

    return arr.map(correct);

}
alert(normalize(['adam', 'LISA', 'barT']));
```                                                                                 
# 高阶函数

JavaScript的函数其实都指向某个变量。既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。

一个最简单的高阶函数：
```
function add(x, y, f) {
    return f(x) + f(y);
}
```
当我们调用add(-5, 6, Math.abs)时，参数x，y和f分别接收-5，6和函数Math.abs，根据函数定义，我们可以推导计算过程为：
```
x = -5;
y = 6;
f = Math.abs;
f(x) + f(y) ==> Math.abs(-5) + Math.abs(6) ==> 11;
return 11;
```
用代码验证一下：
```
'use strict';

function add(x, y, f) {
    return f(x) + f(y);
}
var x = add(-5, 6, Math.abs); // 11
console.log(x);
```
编写高阶函数，就是让函数的参数能够接收别的函数。