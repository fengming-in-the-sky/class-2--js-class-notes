'use strict';


/* 
求数组中[2,33,6,77,8,66]的最小值

*/


var arr=[2,33,6,77,8,66]; //声明一个数组

var min=arr[0];//声明一个最小值 min

for(var i=1;i<arr.length;i++ ) {  //遍历这个数组，把里面每个数组元素和min作比较
    if(arr[i]< min)
    {
        min=arr[i]; //如果这个数组元素大于min 就把这个数组元素存到min
    }
}
    console.log('该数组最小的是' +min); //最后输出这个min


    /* 
求数组中[2,33,6,77,8,66]的最大值

*/
 
var arr=[2,33,6,77,8,66];
var max=arr[0];

for(var i=1;i<arr.length;i++){
    if(arr[i] > max){
        max=arr[i];
    }

}

console.log("该数组最大的是" +max);

/*

计算给定数组 arr 中所有元素的总和
输入描述:
数组中的元素均为 Number 类型
示例1
输入

[ 1, 2, 3, 4 ]
输出

10

function sum(arr) {

}
*/



function sum(arr) {
   return arr.reduce(function (total,num) {
    return total + num;       
   })
}


console.log(sum([ 1, 2, 3, 4 ]))



/*
1、题目描述
找出元素 item 在给定数组 arr 中的位置
输出描述:
如果数组中存在 item，则返回元素在数组中的位置，否则返回 -1
示例1
输入
[ 1, 2, 3, 4 ], 3
输出
2


*/

//indexOf 返回某个元素的位置
function indexOf(arr, item) {
    return arr.indexOf(item);
}
    console.log(indexOf([ 1, 2, 3, 4 ], 3));

/*
3、题目描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4, 2], 2
输出

[1, 3, 4]


*/

//

function remove(arr, item) {
   var result=[];
   arr.forEach(function  (arrItem) {
       if (item != arrItem) {
           result.push(arrItem)
       }
   })
   return result;
}

console.log(remove([1, 2, 3, 4, 2], 2));
/*
4、题目描述
移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果返回
示例1
输入

[1, 2, 2, 3, 4, 2, 2], 2
输出

[1, 3, 4]


*/
function removeWithoutCopy(arr, item) {
 while (arr.indexOf(item) != -1) {
    arr.splice(arr.indexOf(item),1)
 } 
 return arr;
}
        
    console.log(removeWithoutCopy([1, 2, 2, 3, 4, 2, 2], 2));
/*
5、题目描述
在数组 arr 末尾添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4],  10
输出

[1, 2, 3, 4, 10]

function append(arr, item) {
    
}
*/

function append(arr, item) {
     var result=[];
     arr.forEach(function  (arrItem) {
         result.push(arrItem);
     })
     result.push(item);
     return result;
}
    
   

    console.log(append([1, 2, 3, 4],  10));

/*
6、题目描述
删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 2, 3]

function truncate(arr) {

}
*/
function truncate(arr) {
     var result=arr.slice(0);
      return  result.splice(0,arr.length-1)
       
    }
    console.log(truncate([1, 2, 3, 4]));
/*
7、题目描述
在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 10
输出

[10, 1, 2, 3, 4]

function prepend(arr, item) {

}
*/

function prepend(arr, item) {
  return[item].concat(arr)
}
        console.log(prepend([1, 2, 3, 4], 10));
/*
8、题目描述
删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[2, 3, 4]

function curtail(arr) {

}
*/

function curtail(arr) {
 var result=arr.slice(0);
 result.shift();
 return result;
}


console.log(curtail([1, 2, 3, 4]))


/*
9、题目描述
合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], ['a', 'b', 'c', 1]
输出z

[1, 2, 3, 4, 'a', 'b', 'c', 1]

function concat(arr1, arr2) {

}
*/
function concat(arr1, arr2) {
 var currentArr1=arr1.slice(0);
 var currentArr2=arr2.slice(0);

   return currentArr1.concat(currentArr2);
}
console.log(concat([1, 2, 3, 4], ['a', 'b', 'c', 1]))
/*
10、题目描述
在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4], 'z', 2
输出

[1, 2, 'z', 3, 4]

function insert(arr, item, index) {

}
*/

function insert(arr, item, index) {
      var result=arr.slice(0);
      result.splice(index,0,item);
      return result;  
}

console.log(insert([1, 2, 3, 4], 'z', 2))
/*
11、题目描述
统计数组 arr 中值等于 item 的元素出现的次数
示例1
输入

[1, 2, 4, 4, 3, 4, 3], 4
输出

3

function count(arr, item) {

}
*/
function count(arr, item) {
    var num=0;
    arr.forEach(function  (arrItem) {
        if (arrItem === item) {
            num++;
        }
    })
    return num;
}
    console.log(count([1, 2, 4, 4, 3, 4, 3], 4))
/*
12、题目描述
找出数组 arr 中重复出现过的元素
示例1
输入

[1, 2, 4, 4, 3, 3, 1, 5, 3]
输出

[1, 3, 4]

function duplicates(arr) {

}
*/
function duplicates(arr) {
     var result=[];
     arr.forEach(function  (item) {
         if (arr.indexOf(item) != arr.lastIndexOf(item) && result.indexOf(item) == -1) {
             result.push(item)
         }
     })
     return result;
     }


console.log(duplicates([1, 2, 4, 4, 3, 3, 1, 5, 3]))
/*
13、题目描述
为数组 arr 中的每个元素求二次方。不要直接修改数组 arr，结果返回新的数组
示例1
输入

[1, 2, 3, 4]
输出

[1, 4, 9, 16]

function square(arr) {

}
*/

function square(arr) {
   return arr.map(item =>{
      return item * item
  })
}
console.log(square([1, 2, 3, 4]))
/*
14、题目描述
在数组 arr 中，查找值与 item 相等的元素出现的所有位置
示例1
输入

['a','b','c','d','e','f','a','b','c'] 'a'
输出

[0, 6]

function findAllOccurrences(arr, target) {

}
*/
function findAllOccurrences(arr, target) {
    var result=[];
    for (let i = 0; i < arr.length; i++) {
         if (arr[i] === target) {
             result.push(i)
         }     
    }
    return result;
}
console.log(findAllOccurrences(['a','b','c','d','e','f','a','b','c'] ,'a'))
/*
15、题目描述
判断 val1 和 val2 是否完全等同

function identity(val1, val2) {

}
*/

function identity(val1, val2) {
   return val1 === val2;
 }
console.log(identity(1,'1'));
/*
16、题目描述
定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径；
pi: 表示π的值，如果不传，则默认3.14

function area_of_circle(r, pi) {

}
*/
function area_of_circle(r, pi) {
	 if (arguments.length === 1) {
         pi=3.14;
     }
     return r*r*pi;
}
// 测试:
if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}
