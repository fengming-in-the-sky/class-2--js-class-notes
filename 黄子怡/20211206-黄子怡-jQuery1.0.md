
## 表单相关
对于表单元素，jQuery对象统一提供val()方法获取和设置对应的value属性;
针对表单元素，jQuery还有一组特殊的选择器：
:input：可以选择<input>，<textarea>，<select>和<button>；

:file：可以选择<input type="file">，和input[type=file]一样；

:checkbox：可以选择复选框，和input[type=checkbox]一样；

:radio：可以选择单选框，和input[type=radio]一样；

:focus：可以选择当前输入焦点的元素，例如把光标放到一个<input>上，用$('input:focus')就可以选出；

:checked：选择当前勾上的单选框和复选框，用这个选择器可以立刻获得用户选择的项目，如$('input[type=radio]:checked')；

:enabled：可以选择可以正常输入的<input>、<select> 等，也就是没有灰掉的输入；

:disabled：和:enabled正好相反，选择那些不能输入的。
此外，jQuery还有很多有用的选择器，例如，选出可见的或隐藏的元素：
$('div:visible'); // 所有可见的div
$('div:hidden'); // 所有隐藏的div
练习
针对如下HTML结构：
```
<!-- HTML结构 -->

<div class="test-selector">
    <ul class="test-lang">
        <li class="lang-javascript">JavaScript</li>
        <li class="lang-python">Python</li>
        <li class="lang-lua">Lua</li>
    </ul>
    <ol class="test-lang">
        <li class="lang-swift">Swift</li>
        <li class="lang-java">Java</li>
        <li class="lang-c">C</li>
    </ol>
</div>

选出相应的内容并观察效果：

'use strict';
var selected = null;
```
答案：
```
// 分别选择所有语言，
  var selected = $('div.test-selector');

所有动态语言，
 var selected = $('ul.test-lang');
  selected.find('li').css('list-style-type','none');

所有静态语言，
  var selected = $('ol.test-lang');

JavaScript，Lua，C等:
  var selected = $('div .lang-lua,div .lang-javascript,div .lang-c');

// 高亮结果:
if (!(selected instanceof jQuery)) {
    return console.log('不是有效的jQuery对象!');
}
$('#test-jquery').find('*').css('background-color', '');
selected.css('background-color', '#ffd351');
 
JavaScript
Python
Lua
Swift
Java
C
```
## 查找和过滤
通常情况下选择器可以直接定位到我们想要的元素，但是，当我们拿到一个jQuery对象后，还可以以这个对象为基准，进行查找和过滤。最常见的查找是在某个节点的所有子节点中查找，使用find()方法，它本身又接收一个任意的选择器。例如如下的HTML结构：
```
<!-- HTML结构 -->
  <ul class="lang">
        <li class="js dy">JavaScript</li>
        <li class="dy">Python</li>
        <li id="swift">Swift</li>
       
        <ul>
            <li class="dy">Scheme</li>
            <li name="haskell">Haskell</li>
        </ul>
    </ul>
用find()查找：
 var ul = $('ul.lang'); // 获得<ul>

  var dy = ul.find('.dy'); // 获得JavaScript, Python, Scheme
  var swf = ul.find('#swift')[0]; // 获得Swift
  var hsk = ul.find('[name=haskell]')[0]; // 获得Haskell
 
   console.log(dy);
   console.log(swf);
   console.log(hsk);
```

如果要从当前节点开始向上查找，使用parent()方法:
```
var swf = $('#swift'); // 获得Swift
var parent = swf.parent(); // 获得Swift的上层节点<ul>
var a = swf.parent('.red'); // 获得Swift的上层节点<ul>，同时传入过滤条件。如果ul不符合条件，返回空jQuery对象
```
如果位于同一层级的节点，可以通过next()和prev()方法，他只会找下一个或上一个节点 不会跳层例如：
```
var swift = $('#swift');

swift.next(); // Scheme
swift.next('[name=haskell]'); // 空的jQuery对象，因为Swift的下一个元素Scheme不符合条件[name=haskell]

swift.prev(); // Python
swift.prev('.dy'); // Python，因为Python同时符合过滤器条件.dy
```
filter()方法可以过滤掉不符合选择器条件的节点.
map()方法把一个jQuery对象包含的若干DOM节点转化为其他对象：
如下所例：
```
 var langs = $('ul.lang li'); 
   
  var res=langs.map(function () {
       return this.innerText;
    }).get();

   console.log(res);   // 拿到JavaScript, Python, Swift, Scheme和Haskell
 

  var a = langs.filter('.dy');  //滤掉不符合选择器条件的节点
   for(var i=0;i<a.length;i++){
    console.log(a[i].innerText);    //输出JavaScript, Python, Scheme
   }
```

或者传入一个函数，要特别注意函数内部的this被绑定为DOM对象，不是jQuery对象：
```
var a = langs.filter(function(){
     return this.innerText.indexOf('S')===0;
  }); 
  
  for(var i=0;i<a.length;i++){
    console.log(a[i].innerText);
  }
```

练习
对于下面的表单：
```
<form id="test-form" action="#0" onsubmit="return false;">
    <p><label>Name: <input name="name"></label></p>
    <p><label>Email: <input name="email"></label></p>
    <p><label>Password: <input name="password" type="password"></label></p>
    <p>Gender: <label><input name="gender" type="radio" value="m" checked> Male</label> <label><input name="gender" type="radio" value="f"> Female</label></p>
    <p><label>City: <select name="city">
    	<option value="BJ" selected>Beijing</option>
    	<option value="SH">Shanghai</option>
    	<option value="CD">Chengdu</option>
    	<option value="XM">Xiamen</option>
    </select></label></p>
    <p><button type="submit">Submit</button></p>
</form>
输入值后，用jQuery获取表单的JSON字符串，key和value分别对应每个输入的name和相应的value，例如：`{"name":"Michael","email":...}
```
答案：
```
  'use strict';
  var res = {};
  res.Name=$('input[name="name"]').val();
  res.Email=$('input[name="email"]').val();
  res.Password=$('input[name="password"]').val();
  res.Gender=$('input[name="gender"]').val();
  res.City=$('select[name="city"]').val();
  
  var json=JSON.stringify(res,null,' ');

  // 显示结果:
  if (typeof (json) === 'string') {
    console.log(json);
  }
  else {
    console.log('json变量不是string!');
  }
  ```
## 操作DOM
修改Text和HTML
text() 获取节点的文本
html()方法 获取原始HTML文本，例如，如下的HTML结构：
```
  <ul id="test-ul">
    
        <li name="book">Java &amp; JavaScript</li>
    </ul>

//js
   var res=$('#test-ul li[name=book]').text(); // 'Java & JavaScript'
  var res1=$('#test-ul li[name=book]').html(); // 'Java &amp; JavaScript'
    
  console.log(res);
  console.log(res1);
然后我们修改一下文本
'use strict';
var j1 = $('#test-ul li.js');
var j2 = $('#test-ul li[name=book]');
j1.html('<span style="color: red">JavaScript</span>');
j2.text('JavaScript & ECMAScript');  
```

## 修改CSS
```
<!-- HTML结构 -->
<ul id="test-css">
    <li class="lang dy"><span>JavaScript</span></li>
    <li class="lang"><span>Java</span></li>
    <li class="lang dy"><span>Python</span></li>
    <li class="lang"><span>Swift</span></li>
    <li class="lang dy"><span>Scheme</span></li>
</ul>
要高亮显示动态语言，调用jQuery对象的css('name', 'value')方法，我们用一行语句实现：
  'use strict';
    var res=$('#test-css li.dy>span');
    res.css('background-color', '#ffd351').css('color', 'red');
```

为了和JavaScript保持一致，CSS属性可以用'background-color'和'backgroundColor'两种格式。
css()方法将作用于DOM节点的style属性，具有最高优先级。如果要修改class属性，可以用jQuery提供的下列方法：
var div = $('#test-div');
div.hasClass('highlight'); // false， class是否包含highlight
div.addClass('highlight'); // 添加highlight这个class
div.removeClass('highlight'); // 删除highlight这个class
练习：分别用css()方法和addClass()方法高亮显示JavaScript：
```
<!-- HTML结构 -->
<style>
.highlight {
    color: #dd1144;
    background-color: #ffd351;
}
</style>

<div id="test-highlight-css">
    <ul>
        <li class="py"><span>Python</span></li>
        <li class="js"><span>JavaScript</span></li>
        <li class="sw"><span>Swift</span></li>
        <li class="hk"><span>Haskell</span></li>
    </ul>
</div>
//addClass()方法
'use strict';
  var div = $('#test-highlight-css');
  var res= div.find('.js');
  res.addClass('highlight');
//Class()方法
   'use strict';
  var div = $('#test-highlight-css');
  var res= div.find('.js');
 res.css('color','red').css('background-color','#ffd351');
 ```
## 显示和隐藏DOM
考虑到显示和隐藏DOM元素使用非常普遍，jQuery直接提供show()和hide()方法，我们不用关心它是如何修改display属性的，总之它能正常工作：
如下例子HTML同上
```
'use strict';
    
  var py=$('li.py');
   console.log(py);

   
   setTimeout(() => {
    py.hide();
   }, 2000);    //两秒后隐藏
  


  setTimeout(function(){
    py.css('color','red').css('fontSize','50px');
    py.show();
    
   },3000)  //三秒后显示
   ```
## 获取DOM信息
利用jQuery对象的若干方法，我们直接可以获取DOM的高宽等信息，而无需针对不同浏览器编写特定代码：


## 添加DOM
要添加新的DOM节点，除了通过jQuery的html()这种暴力方法外，还可以用append()方法，例如：
```
 <div id="test-highlight-css">
            <ul>
                <li class="py" name="编程语言" width="88" type="text"><span>Python</span></li>
                <li class="js"><span>JavaScript</span></li>
                <li class="sw"><span>Swift</span></li>
                <li class="hk"><span>Haskell</span></li>
            </ul>
          
        </div>
//js

  'use strict';
var div=$('#test-highlight-css');
div.append(' 北京<input type="checkbox" id="bj" checked >');//把DOM添加到最后
div.prepend(' <li class="java"><span>Java</span></li>');    //把DOM添加到最前。
 console.log(div);
```

append()把DOM添加到最后，prepend()则把DOM添加到最前。
## 删除节点
要删除DOM节点，拿到jQuery对象后直接调用remove()方法就可以了。如果jQuery对象包含若干DOM节点，实际上可以一次删除多个DOM节点：

var li = $('#test-div>ul>li');
li.remove(); // 所有<li>全被删除
练习
除了列出的3种语言外，请再添加Pascal、Lua和Ruby，然后按字母顺序排序节点：
```
<!-- HTML结构 -->
<div id="test-div">
    <ul>
        <li><span>JavaScript</span></li>
        <li><span>Python</span></li>
        <li><span>Swift</span></li>
    </ul>
</div>
```
//js
```
use strict';
  //除了列出的3种语言外，请再添加Pascal、Lua和Ruby，然后按字母顺序排序节点：

  var ul = $('#test-div>ul');
  //添加
  var arr = ['Pascal', 'Lua', 'Ruby'].map(x => '<li><span>' + x + '</span></li>')
  ul.append(arr);


  var span = $('#test-div>ul span');

  var res = [];

  //获取文本
  span.map(function () {
    var txt = this.innerText;
    res.push(txt);
  });

  //排序
  res.sort();

  //将res的每一个选项都赋给span
  span.map(function (x) {

        this.innerText=res[x];
   
  });


// 测试:
;(function () {
    var s = $('#test-div>ul>li').map(function () {
        return $(this).text();
    }).get().join(',');
    if (s === 'JavaScript,Lua,Pascal,Python,Ruby,Swift') {
        console.log('测试通过!');
    } else {
        console.log('测试失败: ' + s);
    }
})();
 
JavaScript
Python
Swift
```