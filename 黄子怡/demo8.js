
/*

练习
使用jQuery选择器分别选出指定元素：

仅选择JavaScript

仅选择Erlang

选择JavaScript和Erlang

选择所有编程语言

选择名字input

选择邮件和名字input

<!-- HTML结构 -->
<div id="test-jquery">
    <p id="para-1" class="color-red">JavaScript</p>
    <p id="para-2" class="color-green">Haskell</p>
    <p class="color-red color-green">Erlang</p>
    <p name="name" class="color-black">Python</p>
    <form class="test-form" target="_blank" action="#0" onsubmit="return false;">
        <legend>注册新用户</legend>
        <fieldset>
            <p><label>名字: <input name="name"></label></p>
            <p><label>邮件: <input name="email"></label></p>
            <p><label>口令: <input name="password" type="password"></label></p>
            <p><button type="submit">注册</button></p>
        </fieldset>
    </form>
</div>

运行查看结果：

'use strict';

var selected = null;
selected = ???;

// 高亮结果:
if (!(selected instanceof jQuery)) {
    return console.log('不是有效的jQuery对象!');
}
$('#test-jquery').find('*').css('background-color', '');
selected.css('background-color', '#ffd351');
 
JavaScript

Haskell

Erlang

Python

*/
/*
var js=$('#para-1');                               //通过名字查找
var erl=$('.color-red.color-green');               //通过 class查找，既要满足 color-red 又要满足 color-green
var jserl=$('.color-red');                         //class 查找，  只要含有  color-red  的，都会被查找
var allcpl=$('#test-jquery>p');                    // 所有在 名为 ’test-jquery‘  标签下的 p标签
var nameinput=$('input[name="name"]');             // 首先是个  input，然后 name要满足
var nameinput=$('input[name="name"],input[name="email"]');    // 是个input，name是 name 或 email
*/

var a=$('#para-1')
console.log(a);

var b=$(".color-red.color-green")
console.log(b);

var c=$('.color-red')
console.log(c);

var d=$('#test-jquery')
console.log(d);

var e=$('input [name=name]')
console.log(e);

var f =$('input [name=email],input [name=name]')

console.log(f);