'use strict';
/*
练习：如何通过索引取到500这个值：

'use strict';
var arr = [[1, 2, 3], [400, 500, 600], '-'];
var x = ??;

console.log(x); // x应该为500
*/

/*
练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：

'use strict';
var arr = ['小明', '小红', '大军', '阿黄'];
console.log('???');
*/
var arr = ['小明', '小红', '大军', '阿黄'];
arr.sort();
arr.join(',')

console.log('欢迎'+arr.slice(0,3)+'和'+arr[3]+'同学!');
/*
练习 小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：

低于18.5：过轻 18.5-25：正常 25-28：过重 28-32：肥胖 高于32：严重肥胖 用if...else...判断并显示结果：

'use strict';

var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));
var bmi = ???;
if ...
*/
/*
var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));
var bmi = weight/(height**2)
if (bmi<18.5) {
    alert('过轻')
}
else if (bmi >=18.5 && bmi<25) {
    alert('正常')
}
else if (bmi >=25 && bmi <28 ) {
    alert('过重')
}
else if (bmi <=28 && bmi <32) {
    alert('肥胖')
}
else if (bmi >32) {
    alert('严重肥胖')
}


/*
练习 利用for循环计算1 * 2 * 3 * ... * 10的结果：

'use strict';
var x = ?;
var i;
for ...

if (x === 3628800) {
    console.log('1 x 2 x 3 x ... x 10 = ' + x);
}
else {
    console.log('计算错误');
}

*/

/*
练习 请利用循环遍历数组中的每个名字，并显示Hello, xxx!：

'use strict';
var arr = ['Bart', 'Lisa', 'Adam'];
for ...
请尝试for循环和while循环，并以正序、倒序两种方式遍历。
*/
var arr=['Bart', 'Lisa', 'Adam'];

arr.reverse();

for(var i=0;i<arr.length;i++){
    
console.log('Hello，'+arr[i]+'!');
}



/*
请用rest参数编写一个sum()函数，接收任意个参数并返回它们的和：

'use strict';
function sum(...rest) {
   var arr=[];
   arr=rest
   var res=0;
   for(var i=0;i<arr.length;i++){
        res=res+arr[i]
   }
   return res
}

// 测试:
var i, args = [];
for (i=1; i<=100; i++) {
    args.push(i);
}
if (sum() !== 0) {
    console.log('测试失败: sum() = ' + sum());
} else if (sum(1) !== 1) {
    console.log('测试失败: sum(1) = ' + sum(1));
} else if (sum(2, 3) !== 5) {
    console.log('测试失败: sum(2, 3) = ' + sum(2, 3));
} else if (sum.apply(null, args) !== 5050) {
    console.log('测试失败: sum(1, 2, 3, ..., 100) = ' + sum.apply(null, args));
} else {
    console.log('测试通过!');
}
*/


/*
练习 定义一个计算圆面积的函数area_of_circle()，它有两个参数：

r: 表示圆的半径； pi: 表示π的值，如果不传，则默认3.14

'use strict';

function area_of_circle(r, pi) {
    return 0;

}
// 测试:
if (area_of_circle(2) === 12.56 && area_of_circle(2, 3.1416) === 12.5664) {
    console.log('测试通过');
} else {
    console.log('测试失败');
}
*/

/*
练习：利用reduce()求积：

'use strict';

function product(arr) {
    return 0;

}

// 测试:
if (product([1, 2, 3, 4]) === 24 && product([0, 1, 2]) === 0 && product([99, 88, 77, 66]) === 44274384) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
要把[1, 3, 5, 7, 9]变换成整数13579，reduce()也能派上用场：

var arr = [1, 3, 5, 7, 9];
arr.reduce(function (x, y) {
    return x * 10 + y;
}); // 13579
*/ 

/*
练习：不要使用JavaScript内置的parseInt()函数，利用map和reduce操作实现一个string2int()函数：

'use strict';

function string2int(s) {
    return 0;

}

// 测试:
if (string2int('0') === 0 && string2int('12345') === 12345 && string2int('12300') === 12300) {
    if (string2int.toString().indexOf('parseInt') !== -1) {
        console.log('请勿使用parseInt()!');
    } else if (string2int.toString().indexOf('Number') !== -1) {
        console.log('请勿使用Number()!');
    } else {
        console.log('测试通过!');
    }
}
else {
    console.log('测试失败!');
}

*/


/*
练习 请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

'use strict';

function normalize(arr) {
    return [];

}

// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}
小明希望利用map()把字符串变成整数，他写的代码很简洁：

'use strict';

var arr = ['1', '2', '3'];
var r;
r = arr.map(parseInt);

console.log(r);
结果竟然是1, NaN, NaN，小明百思不得其解，请帮他找到原因并修正代码。
*/
function normalize(arr) {
    function correct(word){
        var list='';
        for (let i = 0; i < word.length; i++) {
            if (i === 0) {
                list += word[i].toUpperCase();
            }
            else
            list +=word[i].toLowerCase();
        }
        return list
    }
return arr.map(correct);
}
console.log(normalize(['adam','LISA','barT']))
/*
练习 请尝试用filter()筛选出素数：

'use strict';

function get_primes(arr) {
    return [];

}

// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
*/

/*

例如，在一个Array中，删掉偶数，只保留奇数，可以这么写：

var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var r = arr.filter(function (x) {
    return x % 2 !== 0;
});
r; // [1, 5, 9, 15]


把一个Array中的空字符串删掉，可以这么写：

var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); // 注意：IE9以下的版本没有trim()方法
});
r; // ['A', 'B', 'C']

*/

/*
可以巧妙地去除Array的重复元素：

'use strict';

var r,
    arr = ['apple', 'strawberry', 'banana', 'pear', 'apple', 'orange', 'orange', 'strawberry'];
    
    r = arr.filter(function (element, index, self) {
    
        return self.indexOf(element) === index;
});

console.log(r.toString()); 
*/

