# 一．创建对象
创建一个对象，然后给这个对象新建属性和方法。

```
var box = new Object(); //创建一个Object 对象
box.name = 'Lee'; //创建一个name 属性并赋值
box.age = 100; //创建一个age 属性并赋值
box.run = function () { //创建一个run()方法并返回值
return this.name + this.age + '运行中...';
};
alert(box.run()); //输出属性和方法的值
```
上面创建了一个对象，并且创建属性和方法，在run()方法里的this，就是代表box 对象本身。这种是JavaScript 创建对象最基本的方法，但有个缺点，想创建多个类似的对象，就会产生大量的代码。

为了解决多个类似对象声明的问题，我们可以使用一种叫做工厂模式的方法，这种方法就是为了解决实例化对象产生大量重复的问题。

```
function createObject(name, age) { //集中实例化的函数
var obj = new Object();
obj.name = name;
obj.age = age;
obj.run = function () {
return this.name + this.age + '运行中...';
};
return obj;
}
var box1 = createObject('Lee', 100); //第一个实例
var box2 = createObject('Jack', 200); //第二个实例
alert(box1.run());
alert(box2.run()); //保持独立
```
工厂模式解决了重复实例化的问题，但是它有许多问题，创建不同对象其中属性和方法都会重复建立，消耗内存；还有函数识别问题等等。

# 二．构造函数的方法
构造函数的方法有一些规范：


1）函数名和实例化构造名相同且大写，(PS：非强制，但这么写有助于区分构造函数和
普通函数)；
2）通过构造函数创建对象，必须使用new 运算符。

```
function Box(name, age) { //构造函数模式
this.name = name;
this.age = age;
this.run = function () {
return this.name + this.age + '运行中...';
};
}
var box1 = new Box('Lee', 100); //new Box()即可
var box2 = new Box('Jack', 200);
alert(box1.run());
alert(box1 instanceof Box); //很清晰的识别他从属于Box
```

## 构造函数可以创建对象执行的过程：


1）当使用了构造函数，并且new 构造函数()，那么就后台执行了new Object()；
2）将构造函数的作用域给新对象，(即new Object()创建出的对象)，而函数体内的this 就
代表new Object()出来的对象。
3）执行构造函数内的代码；
4）返回新对象(后台直接返回)。

注：

1）构造函数和普通函数的唯一区别，就是他们调用的方式不同。只不过，构造函数也是函数，必须用new 运算符来调用，否则就是普通函数。

2）this就是代表当前作用域对象的引用。如果在全局范围this 就代表window 对象，如果在构造函数体内，就代表当前的构造函数所声明的对象。

这种方法解决了函数识别问题，但消耗内存问题没有解决。同时又带来了一个新的问题，全局中的this 在对象调用的时候是Box 本身，而当作普通函数调用的时候，this 又代表window。即this作用域的问题。

# 三.原型
我们创建的每个函数都有一个prototype(原型)属性，这个属性是一个对象，它的用途是包含可以由特定类型的所有实例共享的属性和方法。逻辑上可以这么理解：prototype 通过调用构造函数而创建的那个对象的原型对象。使用原型的好处可以让所有对象实例共享它所包含的属性和方法。也就是说，不必在构造函数中定义对象信息，而是可以直接将这些信息添加到原型中。

```
function Box() {} //声明一个构造函数
Box.prototype.name = 'Lee'; //在原型里添加属性
Box.prototype.age = 100;
Box.prototype.run = function () { //在原型里添加方法
return this.name + this.age + '运行中...';
};
```

我们经常把属性（一些在实例化对象时属性值改变的），定义在构造函数内；把公用的方法添加在原型上面，也就是混合方式构造对象（构造方法+原型方式）：

```
var person = function(name){
   this.name = name
  };
  person.prototype.getName = function(){
     return this.name; 
  }
  var zjh = new person(‘zhangjiahao’);
  zjh.getName(); //zhangjiahao
```

## 下面详细介绍原型：

1.原型对象
　　每个javascript对象都有一个原型对象，这个对象在不同的解释器下的实现不同。比如在firefox下，每个对象都有一个隐藏的__proto__属性，这个属性就是“原型对象”的引用。

2.原型链
　　由于原型对象本身也是对象，根据上边的定义，它也有自己的原型，而它自己的原型对象又可以有自己的原型，这样就组成了一条链，这个就是原型链，JavaScritp引擎在访问对象的属性时，如果在对象本身中没有找到，则会去原型链中查找，如果找到，直接返回值，如果整个链都遍历且没有找到属性，则返回undefined.原型链一般实现为一个链表，这样就可以按照一定的顺序来查找。

1）__proto__和prototype
JS在创建对象（不论是普通对象还是函数对象）的时候，都有一个叫做__proto__的内置属性，用于指向创建它的函数对象的原型对象prototype。以上面的例子为例：
```
console.log(zjh.__proto__ === person.prototype) //true
```
同样，person.prototype对象也有__proto__属性，它指向创建它的函数对象（Object）的prototype
```
console.log(person.prototype.__proto__ === Object.prototype) //true
```
继续，Object.prototype对象也有__proto__属性，但它比较特殊，为null
```
console.log(Object.prototype.__proto__) //null
```

2）constructor
  原型对象prototype中都有个预定义的constructor属性，用来引用它的函数对象。这是一种循环引用
 
```
person.prototype.constructor === person //true
Function.prototype.constructor === Function //true
Object.prototype.constructor === Object //true
```
3）为加深对理解，我们再举一个例子：

```
function Task(id){  
    this.id = id;  
}  
    
Task.prototype.status = "STOPPED";  
Task.prototype.execute = function(args){  
    return "execute task_"+this.id+"["+this.status+"]:"+args;  
}  
    
var task1 = new Task(1);  
var task2 = new Task(2);  
    
task1.status = "ACTIVE";  
task2.status = "STARTING";  
    
print(task1.execute("task1"));  
print(task2.execute("task2"));

结果：

execute task_1[ACTIVE]:task1
execute task_2[STARTING]:task2
```


 # class的继承

 ## class的基本语法
class，也就是我们常说的类，
```
// class
class Person {
  constructor(name, age) {
    this.name = name
    this.age = age
  }
  sayName() {
    console.log(this.name)
  }
}
const p1 = new Person('p1', 18)

// 组合使用构造函数模式和原型模式
function Animal(name, age) {
  this.name = name
  this.age = age
}
Animal.prototype.sayName = function() {
  console.log(this.name)
}
const a1 = new Animal('a1', 18)

console.log(p1, a1)
```

可以看到，通过class生成的实例和通过组合使用构造函数模式和原型模式所生成的实例是一样的。

默认存在constructor方法，该方法默认返回实例对象(即this，也就是说constructor里面的this指向类的实例)。

类的所有方法都定义在类的prototype上
必须通过new来调用class
类里面的方法不需要用逗号隔开，且是不可枚举的
类默认使用严格模式，且不存在提升机制，也就是说在类定义之前使用它是不行的
get和set关键字可对属性的存取行为进行拦截(该属性不用显示地定义)
```
class Person {
 constructor(name, age) {
   this.name = name
   this.age = age
 }
 get Sex() {
   console.log('sex')
   return 'sex'
 }
 set Sex(val) {
   console.log(this.name + this.age + val)
 }
}
const p1 = new Person('boy', 18)
p1.Sex = '南'
console.log(p1)
```

## 静态方法
静态方法指的是通过类直接可以调用并且不被实例所继承的方法。定义静态方法的步骤就是直接在方法前面加是static。
```
class Person {
  constructor(name, age) {
    this.name = name
    this.age = age
  }
  static sayName() {
    console.log('静态方法')
  }
}
const p1 = new Person('boy', 18)
p1.Sex = '南'
console.log(p1)
```


## 静态属性(ES6 明确规定，Class 内部只有静态方法，没有静态属性)，静态属性是指class本身的属性，在声明类之后再添加静态属性，如：

虽然规定了内部没有静态属性，不过也出现了这方面的提案，就是在类的内部通过static关键字定义静态属性，如：

class就先了解到这里，其他的知识点不展开，进入主题。


class的继承围绕三点进行展开描述：如何实现继承，super，多重继承。

## 1、如何实现继承
class通过关键字extends实现继承。具体操作步骤如下：
```
class Person {
  constructor(name, age) {
    this.name = name
    this.age = age
  }
  sayName() {
    console.log('Person')
  }
}

class Animal extends Person {
  
}
const a = new Animal()
a.sayName() // Person
```
需要注意的点：

子类必须在constructor方法中调用super方法
上面提到过，constructor会默认添加，这里super也会默认添加，所以上面的例子会正常执行。
为什么必须要调用super方法呢？
这是因为这里的继承是先塑造父类，再塑造子类。也就是先将父类的东西都拿过来之后，再进行子类的添加。而super负责的就是将父类的东西拿过来。所以必须先调用super。
需要在调用super之后才能使用this。也就是说在子类构造函数constructor里面，super函数要放在最上面。为什么要这样呢？
刚才提到过，class的继承是先将父类拿过来再进行子类的添加，之前在讲借用构造函数实现继承的时候也提到过，如果想要给自身添加属性和方法，需要在调用call或者apply之后添加。否则如果存在同名属性，后面的会覆盖前面的。这里其实也是同样的道理。

## 2、super
class里面的super有两种使用方式，一种是当方法用，一种是当对象用。

当方法用
当方法用就像上面提到的，在constructor里面调用，负责将父类的实例属性拿过来。这个时候，super里面的this指向的是子类。为什么这样呢？其实和借用构造函数实现继承是一样的，子类继承超类的实例属性时，就是通过call或者apply在子类调用超类构造函数。这个时候，this自然指向的就是子类。还要注意一点 的是，其实上面也提到了，当方法用的时候，super必须放到constructor构造函数的最上层
当对象用
上一点说了，当方法用是为了将父类的实例属性继承过来(其实就是复制一份过来)。实例属性是拿到了，但方法呢，比如说有时候子类的方法想要调用父类的某个方法，这个时候要如何做？
```
class Person {
   constructor(name, age) {
     this.name = name
     this.age = age
   }
   sayName() {
     console.log('Person')
   }
   static speak() {
     console.log('speak')
   }
 }
 class Animal extends Person {
   sayName() {
     super.sayName() // Person
   }
   static speak() {
     super.speak() // speak
   }
 }
 const a = new Animal()
 a.sayName()
 Animal.speak()
```
这个时候，super作为对象的所用就体现出来了，通过它可以调用父类的方法(包括静态方法)。
注意，不同类型的方法要在不同类型的方法里面调用。有点绕，其实就是子类的静态方法里面通过super只能调用父类的静态方法，不能调用父类的原型方法。同理，子类的原型方法通过super也只能调用父类的原型方法，不能调用父类的静态方法。
有点像组合继承，super方法就是借用构造函数继承，继承实例属性，super对象就是原型链继承，继承原型上的属性和方法。也就是说super对象是无法访问父类的实例属性的。
那么还有一个问题，通过super调用父类的方法，那么该方法内的this指向的是谁呢？
```
class Person {
  constructor() {
    this.name = 'Person'
  }
  sayName() {
    console.log(this.name)
  }
}
class Animal extends Person {
  constructor() {
    super()
    this.name = 'Animal'
  }
  sayName() {
    super.sayName() // Person
  }
}
```

结果就是，this指向的是当前类(子类)的实例。所以我们通过super对某个属性进行修改，修改的就是子类实例的属性。

## 3、多重继承
有些面向对象编程语言是支持多重继承(即一个子类继承多个父类)，如c++， py。java不支持多继承，但可以通过实现多接口或者内部类的方式实现类似的效果。es6的class继承本质上还是基于原型链的继承，所以也是不支持多继承的。但就像java那样，我们也可以通过其他方式达到相同或者类似的效果。
```
// class
class Person {
  constructor() {
    this.type = 'Person'
    this.age = '18'
  }
  sayName() {
    console.log('说话')
  }
}
class Animal {
  constructor() {
    this.type = 'Animal'
    this.age = '19'
  }
  eat() {
    console.log('进食')
  }
}

// mixin
// 多重继承 一个子类继承多个父类
function mixin(...mixins) {
  class Mix {
    constructor() {
      for(let mixin of mixins) {
        // 拷贝实例属性
        copyProperties(this, new mixin())
      }
    }
  }
  for(let mixin of mixins) {
    copyProperties(Mix, mixin) // 拷贝静态属性
    copyProperties(Mix.prototype, mixin.prototype) // 拷贝原型属性
  }
  return Mix
}
function copyProperties(target, source) {
  // Reflect.ownKeys 返回所有属性key
  // Object.keys 返回属性key，不包括不可枚举属性
  for(let key of Reflect.ownKeys(source)) {
    if (key !== 'constructor' && key !== 'prototype' && key !== 'name') {
      // Object.getOwnPropertyDescriptor 返回指定对象上一个自有属性对应的属性描述符。
      // 自有属性指的是直接赋予该对象的属性，不需要从原型链上进行查找的属性
      // 属性描述符指的是configurable、enumerable、writable、value这些
      const desc = Object.getOwnPropertyDescriptor(source, key)
      Object.defineProperty(target, key, desc)
    }
  }
}
class Other extends mixin(Animal, Person) {}
const oo = new Other()
console.log(oo)
```