# 函数

函数是由事件驱动的或者当它被调用时执行的可重复使用的代码块。

函数分为普通函数、方法、构造函数

##定义和调用

定义函数
```
在JavaScript中，定义函数的方式如下：

function abs(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
```
上述abs()函数的定义如下：

function指出这是一个函数定义；
abs是函数的名称；
(x)括号内列出函数的参数，多个参数以,分隔；
{ ... }之间的代码是函数体，可以包含若干语句，甚至可以没有任何语句。
请注意，函数体内部的语句在执行时，一旦执行到return时，函数就执行完毕，并将结果返回。因此，函数内部通过条件判断和循环可以实现非常复杂的逻辑。

如果没有return语句，函数执行完毕后也会返回结果，只是结果为undefined。

由于JavaScript的函数也是一个对象，上述定义的abs()函数实际上是一个函数对象，而函数名abs可以视为指向该函数的变量。

因此，第二种定义函数的方式如下：
```
var abs = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};
```
在这种方式下，function (x) { ... }是一个匿名函数，它没有函数名。但是，这个匿名函数赋值给了变量abs，所以，通过变量abs就可以调用该函数。

上述两种定义完全等价，注意第二种方式按照完整语法需要在函数体末尾加一个;，表示赋值语句结束。

## 调用

虽然两种定义方式的调用都是一样的，但是还是有一些区别的

声明式函数： 调用可以在 定义之前或者定义之后
```
// 可以调用
fn()
// 声明式函数

function fn() {
	console.log('我是 fn 函数')
}

// 可以调用 fn函数
fn()

```
赋值式函数：只能在定义之后

// 会报错 
fn()

// 赋值式函数
var fn = function() {
	console.log('我是 fn 函数')
}
// 可以调用
fn()
```

自执行函数:不需要调用，可以直接执行

```
;(function(){
            console.log("自执行函数")
})();
```

## 形参与实参的作用
1. 形参

形参实际上就是函数内部使用的变量，在函数外部不能使用

在定义函数()中每写一个单词，就相当于在函数内部定义一个可以使用的变量，多个单词之前使用,隔开
```
// 书写一个参数
function fn(num) {
	// 在函数内部就可以使用 num 这个变量 
}
var fn1 = function(num) {
	// 在函数内部就可以使用 num 这个变量 
}
// 书写两个参数
function fun(num1, num2) {
	// 在函数内部就可以使用 num1 和 num2 这两个变量 
}
var fun1 = function(num1, num2) {
	// 在函数内部就可以使用 num1 和 num2 这两个变量 
}

```
如果只有形参，没有赋值，那么在函数内部使用的时候会出现undefined

形参的值由函数调用时实参决定的

2.实参
在函数调用时为形参赋值使用

多个参数的时候需要一一对应

## arguments关键字的用法

　argument是JavaScript中的一个关键字，用于指向调用者传入的所有参数。
```
function example(x){
    alert(x); //1
    alert(arguments.length); //3
    for(var i=0; i<arguments.length; i++){
        alert(arguments[i]);  //1,2,3   
    }      
}
example(1,2,3);
```

即使不定义参数，也可以取到调用者的参数。

```
function abs() {
    if (arguments.length === 0) {
        return 0;
    }
    var x = arguments[0];即使不定义参数，也可以取到调用者的参数。

复制代码
function abs() {
    if (arguments.length === 0) {
        return 0;
    }
    var x = arguments[0];
    return x >= 0 ? x : -x;
}

abs(); // 0

abs(10); // 10

abs(-9); // 9

    return x >= 0 ? x : -x;
}

abs(); // 0

abs(10); // 10

abs(-9); // 9
```

