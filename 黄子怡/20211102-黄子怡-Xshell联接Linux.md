# 如何用Xshell连接本地虚拟机(我也不会sorry)

Xshell连接linux主机了，点击最左边的加号图标，新建一个服务器连接(或者使用文件 – 新建)

![图裂了](../img/1.jpg)

在主机一栏填写自己的服务器ip地址，端口没有修改过的话一般就是22端口，填写完成后点击确定

![图裂了](../imgs/2.jpg)


添加完服务器后我们点击加号右边的打开按钮，选择刚刚添加的”新建会话”

![图裂了](../imgs/3.jpg)

用户名填写root，如果想要记住密码的话勾选记住用户名

![图裂了](../imgs/4.jpg)

然后选择Password输入服务器密码，如果想要记住密码的话勾选记住密码

![图裂了](../imgs/5.jpg)

出现如下的提示，证明服务器连接成功；如果连接中断，则检查ip、端口、账号、密码等参数

![图裂了](../imgs/6.jpg)

输入命令，测试连接

![图裂了](../imgs/7.jpg)


# 常用命令
最最常用：Tab键的提示作用。



## 命令mkdir-建立目录

mkdir test 在当前目录中建立名为test的目录

mkdir -p test/test1/test2/test3  在当前目录下建立嵌套子目录



## 命令cd-更改目录

cd 切换到主目录

cd ~切换到主目录

cd / 切换到根目录

cd ..  切换到上一级目录

cd test 切换到当前目录下的test目录



## 命令ls-列出文件

ls 显示当前目录文件

ls -la 给出当前目录下所有文件的一个长列表，包括隐藏文件。

ls  a*  列出当前目录下以字母a开头的所有文件

ls  *.doc  列出当前目录下以.doc结尾的所有文件



## 命令cp-复制文件或目录

cp test.txt test.doc 把文件test.txt复制为新文件test.doc

cp test.txt  test  把文件afile从当前目录复制到test目录下

cp * test 把当前目录下的所有未隐藏文件复制到test目录下

cp -a test test.bak  递归性地把当前目录下的test目录复制为新目录test.bak,保持文件属性，并复制所有的文件，包括隐藏文件。

cp -i  在覆盖前询问用户

cp -v   告诉用户正在做什么



## 命令mv-移动和重命名文件或目录

mv test test1  将目录test重命名为test1，如果已有test1目录，则将test移动到test1目录

mv test.txt test1.txt  将文件test.txt重命名为test1.txt



## 命令rm-删除文件和目录

rm *  删除当前目录下的所有文件（未隐藏文件），rm命令不删除目录，除非也指定了-r （递归）

rm -rf test  删除test目录以及它所包含的所有内容

rm -i  在删除前询问用户



## 命令more、less-查看文件内容

more/less  test.txt 查看test.txt的内容



## 命令grep-搜索文件内容

grep test test.txt 在文件test.txt中查找包含test的所有行

tail -2 test.txt|grep test 在test.txt文件的后两行中查找包含test的行

tail test.txt|grep ^v test  在test.txt文件查找没有test的行



## 命令find-查找文件

find *查找当前目录下所有文件

find *|grep test查找当前目录下包含test的文件



## 命令vi-编辑文件

vi 11.txt 用vi编辑文件11.txt

insert 进行修改

shift+;->x  退出

ctrl+z 将vim挂起（暂停），暂停后可进行其他shell操作，完了之后可通过fg命令切换回vim界面继续编辑。



## 命令cat-显示文件内容

cat test.txt  显示test.txt的内容

## 命令kill-杀掉进程

kill -9 pid 强制删除指定pid的进程。（同时杀多个进程号为pid的进程中间以空格分割）

更多：

-1：重新读取一次参数的设定档（类似reload）

-15：以正常的程序方式终止一项工作。与-9是不一样的

扩展：杀掉jboss进程后，启动命令

首先要找到jboss的bin目录：举例 cd /usr/local/jboss6.2/bin

输入命令：

nohup ./standalone.sh&

Tomcat的操作大同小异。进入到tomcat的bin中

输入命令：

sudo ./run.sh



## 命令tail-查看文件详细信息

tail -f  test.txt 看test.txt文件的详细信息

拓展：查看jboss日志，进入到jboss的bin中，然后输入命令：

tail -f nohup.out



## 命令diff -比较文件内容

diff test1 test2  比较目录test1和目录test2的文件列表是否相同，但不比较文件的实际内容，不同则列出

diff test1.txt test2.txt 比较test1.txt和test2.txt的内容是否相同，将不相同的内容显示

comm test1.txt test2.txt 比较文件，显示两个文件不相同的内容



## 命令touch-创建一个空文件

touch test.txt 创建一个空文件，文件名为test.txt



## 命令man-查看某个命令的帮助

man ls 显示ls命令的帮助内容



## 命令w-显示登录用户的详细信息



## 命令who-显示登录用户



## 命令last-查看最近哪些用户登录系统



## 命令date-系统日期设定

date -s "170207 08:00:00"设置系统日期为2017年2月7日早上 8点整



## 命令clock-时钟设置

clock -r  对系统Bios中读取时间参数

clock -w 将系统时间（如由date设置的时间）写入Bios



## 命令uname-查看系统版本

uname -r显示操作系统内核的version



## 命令reboot、shutdown-关闭和重新启动计算机

reboot 重新启动计算机

shutdown -r now 重新启动计算机，停止服务后重新启动计算机

shutdonw -h now 关闭计算机，停止服务后再关闭系统

halt 关闭计算机



## 命令su-切换用户

su - 切换到root用户

su -   David 切换到David用户



## 命令free-查看内存和swap分区使用情况



## 命令uptime-现在的时间，系统开机运转到现在经过的时间，连线的使用者数量，最近一分钟，五分钟和十五分钟的系统负载。



## 命令vmstat-监视虚拟内存使用情况



## 命令clear-清屏



## 命令df -h

查看磁盘容量及使用情况


原文链接：https://blog.csdn.net/qwlzxx/article/details/54897096